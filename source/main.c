/******************************************************************************
* Copyright (C) 2017, Huada Semiconductor Co.,Ltd All rights reserved.
*
* This software is owned and published by:
* Huada Semiconductor Co.,Ltd ("HDSC").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with HDSC
* components. This software is licensed by HDSC to be adapted only
* for use in systems utilizing HDSC components. HDSC shall not be
* responsible for misuse or illegal use of this software for devices not
* supported herein. HDSC is providing this software "AS IS" and will
* not be responsible for issues arising from incorrect user implementation
* of the software.
*
* Disclaimer:
* HDSC MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS),
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING,
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED
* WARRANTY OF NONINFRINGEMENT.
* HDSC SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT,
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION,
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA,
* SAVINGS OR PROFITS,
* EVEN IF Disclaimer HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED
* FROM, THE SOFTWARE.
*
* This software may be replicated in part or whole for the licensed use,
* with the restriction that this Disclaimer and Copyright notice must be
* included with each copy of this software, whether used in part or whole,
* at all times.
*/
/******************************************************************************/
/** \file main.c
 **
 ** A detailed description is available at
 ** @link Sample Group Some description @endlink
 **
 **   - 2017-05-17  1.0  cj First version for Device Driver Library of Module.
 **
 ******************************************************************************/

/******************************************************************************
 * Include files
 ******************************************************************************/
#include "ddl.h"
#include "uart.h"
#include "gpio.h"
#include "sysctrl.h"
#include "pca.h"
#include "lpm.h"

/******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/
#define     T1_PORT                 (3)
#define     T1_PIN                  (3)  

/******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/******************************************************************************
 * Local type definitions ('typedef')
 ******************************************************************************/

/******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/

static void App_GpioInit(void);
static void App_PcaInit(void);

/******************************************************************************
 * Local variable definitions ('static')                                      *
 ******************************************************************************/
volatile static uint8_t u8RxData;
volatile static uint8_t u8TxCnt=0;
volatile static uint8_t u8RxCnt=0;
/*****************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
void App_UartCfg(void);
void App_PortInit(void);

void PcaInt(void)
{
    if(Pca_GetItStatus(PcaCcf1) == TRUE)
    {
        Pca_ClrItStatus(PcaCcf1);
    }
}

/*
IO初始化
*/

static void App_GpioInit(void)
{
        stc_gpio_cfg_t GpioInitStruct;

    Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE);
    
    //PA07设置为PCA_CH1
    GpioInitStruct.enDrv  = GpioDrvH;
    GpioInitStruct.enDir  = GpioDirIn;
    Gpio_Init(GpioPortA, GpioPin7, &GpioInitStruct);
    Gpio_SetAfMode(GpioPortA,GpioPin7,GpioAf2);

    //PA06设置为PCA_CH0
    GpioInitStruct.enDir  = GpioDirOut;
    Gpio_Init(GpioPortA, GpioPin6, &GpioInitStruct);
    Gpio_SetAfMode(GpioPortA, GpioPin6, GpioAf2);
}

/*
PCA功能
*/
en_result_t PcaCaptest (void)
{
    stc_pcacfg_t  PcaInitStruct;
	  uint16_t u16CcapData[8] = {0};
		
    Sysctrl_SetPeripheralGate(SysctrlPeripheralPca, TRUE);
    volatile en_result_t     enResult = Error;
    volatile uint16_t        u16InitCntData = 0;
    volatile uint16_t        u16Cnt =0;
    PcaInitStruct.pca_clksrc = PcaPclkdiv32;
    PcaInitStruct.pca_cidl   = FALSE;
    PcaInitStruct.pca_ecom   = PcaEcomEnable;       //允许比较器功能
    PcaInitStruct.pca_capp   = PcaCappDisable;      //禁止上升沿捕获
    PcaInitStruct.pca_capn   = PcaCapnDisable;      //禁止下降沿捕获
    PcaInitStruct.pca_pwm    = PcaPwm8bitDisable;    //禁止PWM控制输出
    PcaInitStruct.pca_epwm   = PcaEpwmEnable;       //允许16bitPWM输出
    PcaInitStruct.pca_ccap   = 80;
    PcaInitStruct.pca_carr   = 320;             
    Pca_M0Init(&PcaInitStruct);
		
    PcaInitStruct.pca_cidl   = FALSE;
    PcaInitStruct.pca_ecom   = PcaEcomDisable;       //禁止比较器功能
    PcaInitStruct.pca_capp   = PcaCappEnable;        //允许上升沿捕获
    PcaInitStruct.pca_capn   = PcaCapnEnable;        //允许下降沿捕获
    PcaInitStruct.pca_mat    = PcaMatDisable;        //禁止匹配功能
    PcaInitStruct.pca_tog    = PcaTogDisable;        //禁止翻转控制功能
    PcaInitStruct.pca_pwm    = PcaPwm8bitDisable;    //禁止PWM控制输出
    PcaInitStruct.pca_epwm   = PcaEpwmDisable;       //禁止16bitPWM输出
    Pca_M1Init(&PcaInitStruct);    
    		

    Pca_ClrItStatus(PcaCcf1);
    Pca_ConfModulexIt(PcaModule1, TRUE);
    EnableNvic(PCA_IRQn, IrqLevel3, TRUE);

    Pca_SetCnt(u16InitCntData); 
		Pca_StartPca(TRUE);

		u16Cnt = 256;
		while(u16Cnt--);
		
		Gpio_WriteOutputIO(GpioPortA,GpioPin7,TRUE);
		
		while(1)
		{
			if (Pca_GetItStatus(PcaCcf1) == TRUE);
			{
				u16CcapData[0] = Pca_GetCcap(PcaModule1);
				enResult = Ok;
				break;
			}
		}
			
		u16Cnt = 256;
		while(u16Cnt--);
		Gpio_WriteOutputIO(GpioPortA,GpioPin7,FALSE);
			
		while(1)
		{
			if (Pca_GetItStatus(PcaCcf1)==TRUE);
			{
				u16CcapData[1] = Pca_GetCcap(PcaModule1);
				enResult = Ok;
				break;
			}
		}
		Pca_StartPca(FALSE);
		enResult = Ok;
}
/*
主函数
*/
int32_t main (void)
{
	volatile uint8_t u8TestFlag = 0;
	Sysctrl_SetPeripheralGate(SysctrlPeripheralPca, TRUE);
	Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE);
	Sysctrl_SetPeripheralGate(SysctrlPeripheralUart0,TRUE);
	
	#ifdef DEBUG
    Debug_UartInit();                                         //调试串口初始化
#endif
    
#ifdef DEBUG
    printf("CAP IS %s"，u16CcapData);                           //输出调试内容
#endif  
	
	
	if (Ok != PcaCaptest())
	{
		u8TestFlag |= 0x02;
	}
	while(1);
}



